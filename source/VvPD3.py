import argparse
import colorama

colorama.init()
RED = '\033[31m'
RESET = '\033[0m'


def page(line):
    """Функция проверяет является ли строка вида <k> где k-любое число в диапазоне от 0
    до n.
    :param line: строка ,которую вводит пол-ль в функции user_choice
    :return:если строка является вида <k> возвращает число k
    :return: в противном случае вернет False
    """
    if line != "":
        if line[0] == "<" and line[-1] == ">":
            line = line[1:]
            line = line[:-1]
            if line.isdigit():
                return int(line)
    return False


def check(n, m, k):
    """Функция проверяющая ,что прасеры являются натруальными числами
    Так же проверяет ,чтобы нынешняя страница не была больше ,чем
    число всех всех страниц
    :param n: всего страниц(натур. число)
    :param m: сколько страниц на границе(натур. число)
    :param k: текущая страница(натур. число)
    :return: возвращает True если числа больше 0 и k<n
    :return: возвращает False,если пол-ль ввел символы отличные от цифр или цифры не натуральны или k>n.
    """
    try:
        n = int(n)
        m = int(m)
        k = int(k)
        if n >= k > 0 and n > 0 and m >= 0:
            return True
        else:
            return False
    except ValueError:
        return False


def output_left(n, m, k):
    """Выводит строку ввида
    << < 18 19 (20) или же ввида
    << < 16 17 (18) 19 20
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    """
    a = []
    for i in range(k - m, n + 1):
        if i == k:
            i = f'({i})'
            a.append(i)
        else:
            a.append(i)
    print("\n<<", "<", *a, "\n", sep=' ')


def output_center(n, m, k):
    """Выводит строку ввида
    << < 3 4 (5) 6 7 > >>
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    """
    a = []
    for i in range(k - m, k + m + 1):
        if i == k:
            i = f'({i})'
            a.append(i)
        else:
            a.append(i)
    print("\n<<", "<", *a, ">", ">>\n", sep=' ')


def output_right(n, m, k):
    """Выводит строку ввида
     (1) 2 3 > >> или же ввида
      1 2 (3) 4 5 > >>
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    """
    a = []
    for i in range(1, k + m + 1):
        if i == k:
            i = f"({i})"
            a.append(i)
        else:
            a.append(i)
    print('\n', *a, ">", ">>\n", sep=' ')


def output_all(n, m, k):
    """Выводит строку ввида
     1 2 (3) 4 5
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
     """
    a = []
    for i in range(1, n + 1):
        if i == k:
            i = f'({i})'
            a.append(i)
        else:
            a.append(i)
    print('\n', *a, sep=' ')


def output_error():
    """Функция выводящая строку о неправльном вводе
    RED - меняет цвет строки на красный
    RESET - возвращает обратно цвет на белый
    """
    print(RED + "\n"
                "BAKA!!!\n"
                "Wrong Input" + RESET)


def choice_of_output(n, m, k):
    """Определяет какой ввид имеет строка ,основываясь на трех главных значениях
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    :return: Возвращает строку для запуска одноименной функции
    """
    if k - m <= 1 and k + m < n and k <= n:
        return 'output_right'
    elif k + m >= n >= k and k - m >= 2:
        return 'output_left'
    elif k - m <= 1 and k + m >= n >= k:
        return 'output_all'
    elif k - m > 1 and k + m <= n - 1:
        return 'output_center'
    else:
        return 'output_error'


def what_output(n, m, k):
    """Запсукает функции для вывода строки ,основываясь на трех главных значениях
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    """
    solution = choice_of_output(n, m, k)
    dict_of_choice = {'output_right': lambda a, b, c: output_right(a, b, c),
                      'output_left': lambda a, b, c: output_left(a, b, c),
                      'output_all': lambda a, b, c: output_all(a, b, c),
                      'output_center': lambda a, b, c: output_center(a, b, c),
                      'output_error': lambda: output_error()}
    dict_of_choice[solution](n, m, k)


def user_choice(n, m, k):
    """Функция определяющая выбор пользователя
    Принимает на вход 3 главных значения.Запрашивает ввод от пользователя.После смотрит
    если команда в словаре,то выполняет действие согласно значению ключа.Если команды нет
    в словаре,то выполняет провреку на команду перехода на опред стр.Если все выше не подходит,то
    возвращает не изменненое значени k и выводит строку о неправильном вводе.
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    :return: возвращает измененое значение k ,если команда введена правильно
    """
    my_func = {">": (lambda j, a: j + 1 if j < a else a),
               ">>": (lambda j, a: a),
               "<": (lambda j, a: j - 1 if j > 1 else 1),
               "<<": (lambda j, a: 1)}

    value = input("\nВыберите действие:\n > - на стр вперед "
                  "\n >> - на послед стр "
                  "\n < - на стр назад"
                  "\n << - на послед стр"
                  "\n <k> - переход на k стр"
                  "\n Ваш выбор:")
    if value in my_func:
        k = my_func[value](k, n)
    elif type(page(value)) == int:
        value = page(value)
        if 1 <= value <= n:
            k = value
        else:
            output_error()
    else:
        output_error()
    return k


def infinity_cycle(n, m, k):
    """Заставляет программу работать бесконечно
    :param n: всего страниц(натур. число)
    :param m: кол-во страниц на границах(натур. число)
    :param k: текущая страница(натур. число)
    """
    while True:
        k = user_choice(n, m, k)
        what_output(n, m, k)


def main():
    parser = argparse.ArgumentParser(description='О пагинации')
    parser.add_argument('n', type=str)
    parser.add_argument('k', type=str)
    parser.add_argument('m', type=str)
    args = parser.parse_args()
    n = args.n
    k = args.k
    m = args.m
    if check(n, m, k):
        print(RED + "I ask you.Are you my master?" + RESET)
        n = int(n)
        m = int(m)
        k = int(k)
        what_output(n, m, k)
        infinity_cycle(n, m, k)
    else:
        output_error()


if __name__ == "__main__":
    main()
